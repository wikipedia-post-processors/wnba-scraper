/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');
}


function resetInputErrorMsg() {
  /* if an invalid input message is displayed, it is removed. Otherwise, does nothing */
  const messageElement = document.getElementById('input-error-message');
  messageElement.textContent = '';
}


function clearResults() {
  console.log('clearing results');
  document.getElementById('output').innerHTML = '';
}


function submitQuery() {
  /* handles form submit */
  // if an invalid input message is displayed, remove it
  resetInputErrorMsg();

  // if any previous search's results are displayed, clear them
  clearResults();

  // get form data
  const firstName = document.getElementById('first-name').value;
  const lastName = document.getElementById('last-name').value;
  const year = document.getElementById('year').value;

  console.log('testing submitQuery');

  // build request url
  const baseUrl = window.location.hostname === 'localhost' ? 'http://localhost:3000' : '';  // hostname is automatically used in production
  const route = '/player';
  let reqUrl = baseUrl + route + '?firstname=' + firstName + '&lastname=' + lastName;
  if (year) {
    reqUrl += '&year=' + year;
  }

  // make request and define callback
  makeGET(reqUrl, true, (resData) => {
    displayResult(resData);
  });
}


function displayResult(res) {
  // display stats data
  const yearData = res;

  if (res.years) {
    // we're just displaying a list of available years
    const ul = document.createElement('ul');
    document.getElementById('output').appendChild(ul);
    for (const year of res.years) {
      const li = document.createElement('li');
      li.textContent = year;
      ul.appendChild(li);
    }
    return;
  }

  if (typeof yearData === 'undefined') {
    // there wasn't any yearData. Maybe response format has changed without updating us
    console.log('ERROR trying to display result from response: check format of response');
    return;
  }

  console.log('in displayResult, creating table for ' + yearData);

  // add a new empty table to the output section
  const table = document.createElement('table');
  document.getElementById('output').appendChild(table);

  // add thead with empty header row
  const thead = document.createElement('thead');
  table.appendChild(thead);
  const theadRow = document.createElement('tr');
  thead.appendChild(theadRow);

  // add tbody with empty data row
  const tbody = document.createElement('tbody');
  table.appendChild(tbody);
  
  // add a new tr for each team this year
  for (const teamData of yearData.stats) {
    const tbodyRow = document.createElement('tr');
    tbody.appendChild(tbodyRow);
    console.log('\nthis is a new table row for ' + teamData);

    // add a new dt and th to the table for each stat
    for (const statName of Object.keys(teamData)) {
      console.log('\tthis is a new td for stat ' + statName);
      const statValue = teamData[statName];
      // add a new header to the header row
      const th = document.createElement('th');
      theadRow.appendChild(th);
      th.textContent = statName;
  
      // add a new td to the body row
      const td = document.createElement('td');
      tbodyRow.appendChild(td);
      td.textContent = statValue;  
    }
  }

  // display awards data if available
  if (res.awards[0] !== 'NONE_FOUND') {
    const awards = res.awards;

    // add header
    const awardsHeader = document.createElement('h2');
    awardsHeader.textContent = 'Awards';
    document.getElementById('output').appendChild(awardsHeader);

    // ul of all awards
    const ul = document.createElement('ul');
    document.getElementById('output').appendChild(ul);
  
    for (const award of awards) {
      const li = document.createElement('li');
      ul.appendChild(li);
      li.textContent = award;
    }
  } else {
    const noAwards = document.createElement('p');
    noAwards.textContent = 'no awards received this year';
    document.getElementById('output').appendChild(noAwards);
  }

  // display achievements data if available
  if (res.achievements && res.achievements !== 'NONE_FOUND') {
    const achievements = res.achievements;

    // add header
    const awardsHeader = document.createElement('h2');
    awardsHeader.textContent = 'Achievements';
    document.getElementById('output').appendChild(awardsHeader);

    // ul of all achievements
    const ul = document.createElement('ul');
    document.getElementById('output').appendChild(ul);
  
    for (const achievement of achievements) {
      const li = document.createElement('li');
      ul.appendChild(li);
      li.textContent = achievement;
    }
  }

  console.log('\ndone');
}


function makeGET(url, isAsync, callback) {
  /* makes a GET request to the server and executes callback, which is provided with the response text
  callback: function to execute after successful POST and response from server. Receives parsed response object
  */
  console.log('making GET to ', url);

  let req = new XMLHttpRequest();
  req.open('GET', url);
  req.setRequestHeader('content-type', 'application/json');

  // define callback for response
  if (isAsync) {
    let response;
    req.addEventListener('load', () => {
      if(req.status >= 200 && req.status < 400){
        // if a stats page wasn't available for the given name
        console.log('parsing this response text:\n' + req.responseText);
  
        // if (req.responseText === 'NO_PLAYER_FOUND') {
        //   console.log('server said no. Try a different name');
        //   document.getElementById('input-error-message').textContent = 'No player found. Try a different name';
        //   return;
        // } else if (req.responseText === 'YEAR_NOT_FOUND') {
        //   console.log('server said no. Try a different year');
        //   document.getElementById('input-error-message').textContent = 'No data found for that year. Try a different year';
        //   return;
        // }

        response = JSON.parse(req.responseText);

        // generic robustness
        if (!response) {
          console.log('received a response, but it was undefined');
          return;
        }
        
        if (response.errorMessage === 'NO_PLAYER_FOUND') {
          console.log('server said no. Try a different name');
          document.getElementById('input-error-message').textContent = 'No player found. Try a different name';
          return;
        } else if (response.errorMessage === 'YEAR_NOT_FOUND') {
          console.log('server said no. Try a different year');
          document.getElementById('input-error-message').textContent = 'No data found for that year. Try a different year';
          return;
        }
  
        console.log('received response:\n', response);
        callback(response);
      } else {
        // server responded with error code
        console.log("Error in network request: " + req.statusText);
    }});
  }

  // send the request
  req.send();
}

// form submit handler
document.getElementById('player-search').addEventListener('submit', (event) => {
  event.preventDefault();  // don't want a refresh and then a re-render
  submitQuery();
});

