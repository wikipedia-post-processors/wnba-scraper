/* 
using boilerplate express-handlebars skeleton code, which included error response routes, listen call, 
and generic handlebars setup 
*/
const express = require('express');
const axios = require('axios');
const { query } = require('express');

const app = express();
const handlebars = require('express-handlebars').create({
    defaultLayout:'main',
    partialsDir: ['views/partials/'],
    helpers: {
        log: (something) => {  
        console.log("====================");
        console.log("Handlebars Debugger");
        console.log(something);}
    }
});
app.engine('handlebars', handlebars.engine);  // note: handlebars always looks in /views/layouts for .handlebars
app.use(express.static('views'));
app.set('view engine', 'handlebars');  // Optional to include or omit the .handlebars extension
app.set('port', 3000);


async function getRosterFromWiki(teamCity, teamName, year) {
    /* 
    returns a list of players given teamCity and teamName 
    currently using brute force 
    */
    const context = {};  // return variable. Will have .errorMessage or 

    // get a page like https://en.wikipedia.org/wiki/Cleveland_Cavaliers_all-time_roster
    const teamNameUrlized = urlizePlayerName(teamCity + ' ' + teamName);
    const queryStringBase = 'action=parse&format=json&page=' + teamNameUrlized + '_all-time_roster';
    let queryString = queryStringBase + '&prop=sections';

    const pageSections = await makeWikiApiGet(queryString);
    console.log('made GET with ' + queryString + ' and got:\n', pageSections);

    // if request was undefined, ask user to check the name
    if (!pageSections || typeof pageSections.parse === 'undefined') {
        context.errorMessage = 'NO_TEAM_FOUND';
        return context;
    }

    // get Wiki page index of every alphabetical section of the page (sections with "level": "3")
    context.playersOut = [];
    for (const pageSection of pageSections.parse.sections) {
        if (pageSection.toclevel === 2) {  // alphabetical sections are level 2 in table of contents
            // this is an alphabetical section of players such as "A to B"; get the table section
            queryString = queryStringBase + '&prop=wikitext' + '&section=' + pageSection.index;
            const alphabeticalSection = await makeWikiApiGet(queryString);

            if (!alphabeticalSection.parse || !alphabeticalSection.parse.wikitext) {
                console.log('unexpected response for alphabet sections using: ', queryString);
                console.log('got this response:\n', alphabeticalSection);
                context.errorMessage = 'BAD_PARSE';
                return context;
            }

            // split at row (substrings delimited by '-\n')
            const rowSplitRE = /-\\n/g;  // TODO: not using; ok?
            const rowSplit = alphabeticalSection.parse.wikitext['*'].split('-\n');
            // for each row
            for (const row of rowSplit) {
                // split on {{...}}, which will capture "sortname|firstname|lastname" and "nbay|...", along with junk
                const bracketSplit = row.split('{{');
                // parse Seasons column (ex, 1993-1997) and Player column (name)
                let playerFirstName;
                let playerLastName;
                let startYear;
                let endYear;
                for (const bracketChunk of bracketSplit) {
                    // only interested in "nbay..." and "sortname..."
                    if (bracketChunk.includes('sortname')) {
                        // get player name
                        let pipeSplit = bracketChunk.split('|');  // makes sortname, Lance, Allred}}
                        playerFirstName = pipeSplit[1];                        
                        playerLastName = pipeSplit[2].split('}}')[0];  // cut trailing }}
                    // get start and end years
                    } else if (bracketChunk.includes('nbay')) {
                        if (bracketChunk.includes('full')) {
                            // this is a single season spanning two years, like "nbay|2018|full=y"
                            let pipeSplit = bracketChunk.split('|');
                            startYear = Number(pipeSplit[1]);
                            endYear = startYear + 1;
                        } else if (bracketChunk.includes('start')) {
                            // this is the start year of a range like {{nbay|1997|start}} - {{nbay|1998|end}}
                            let pipeSplit = bracketChunk.split('|');
                            startYear = Number(pipeSplit[1]);
                        } else if (bracketChunk.includes('end')) {
                            // this is the end year of a range like {{nbay|1997|start}} - {{nbay|1998|end}}
                            let pipeSplit = bracketChunk.split('|');
                            endYear = Number(pipeSplit[1]);
                        }
                    }  // else, we don't care
                }
                // if requested year is within this player's year range
                if (year >= startYear && year <= endYear) {
                    // console.log('\t\t***player matched: ' + playerFirstName + ' ' + playerLastName + ' from range ' + startYear + '-' + endYear);
                    // append name to output
                    context.playersOut.push(playerFirstName + ' ' + playerLastName);
                }
            }            
        }
    }

    // return list of player names if any matched
    if (context.playersOut.length === 0) {
        context.errorMessage = 'YEAR_NOT_FOUND';
        return context;
    }

    return context;
}


function urlizePlayerName(firstLast) {
    /*
    replaces spaces with underscores and capitalizes chars that follow spaces
    firstLast: player name, in format "stephen curry" or "Stephen Curry"
    */
    // if a char follows a space, capitalize it
    // snippet from https://stackoverflow.com/a/58254980/14257952
    console.log(firstLast);
    firstLast = firstLast.replace(/(^\w{1})|(\s{1}\w{1})/g, (letter) => {
            return letter.toUpperCase(); 
    });

    // replace spaces with underscores
    firstLast = firstLast.replace(' ', '_');

    console.log('\t-> ', firstLast);

    return firstLast;
}


async function getWikiStats(playerName) {
    /*
    queries the Wikipedia api and returns an object with stats for a player
    playerName: string with player name, in format "stephen curry" (not case-sensitive)
    return: an object with "teams", "steals", etc as keys, or false if a request didn't work
    */
    const playerNameUrlized = urlizePlayerName(playerName);
    const queryStringBase = 'action=parse&format=json';
    /* ==============================================================
        1. get player stats and teams player was on during given year 
    */
    // get sections of player's stats page
    let queryString = queryStringBase + '&prop=sections' + '&page=' + playerNameUrlized;

    // console.log('*** same as working version? ', queryString === 'action=parse&format=json&page=List_of_career_achievements_by_LeBron_James&prop=sections');
    // console.log('\tused: ', queryString, '\nvs correct:', 'action=parse&format=json&page=List_of_career_achievements_by_LeBron_James&prop=sections');

    const pageData = await makeWikiApiGet(queryString);

    // if request was undefined, ask user to check the name
    if (!pageData || typeof pageData.parse === 'undefined') {
        return false;
    }

    console.log('got a player page with this GET: ' + queryString);

    const sections = pageData.parse.sections;

    // Find "line": "Regular season" (or “Playoffs”) and get the value of its “index”
    let sectionIndex_stats;
    let sectionIndex_achievements; // (also, grab the achievements section while we're here)
    let readyForAwards = false;
    for (const section of sections) {
        if (section.line === 'Regular season' && typeof sectionIndex_stats === 'undefined') {
            console.log('\tfound stats index ' + section.index);
            sectionIndex_stats = section.index;
        } else if (section.line === 'Awards and honors') {
            readyForAwards = true;
        } else if (section.line === 'NBA' && readyForAwards === true) {
            console.log('\tfound awards index ' + section.index);
            sectionIndex_awards = section.index;
        }
    }

    // repeat the request to the api but with "prop=wikitext&section=[section index]"
    queryString = queryStringBase + '&page=' + playerNameUrlized + '&prop=wikitext&section=' + sectionIndex_stats;

    const seasonResponse = await makeWikiApiGet(queryString);
    if (!seasonResponse.parse) {
        console.log('undefined response for "regular season" stat table using: ', queryString);
        return false;
    }
    const seasonData = seasonResponse.parse.wikitext['*'];

    // search the response text for "nbay | YYYY" where YYYY is the start year of the season
    const seasonData_years = seasonData.split('nbay|');

    // discard junk before the first occurrence of "nbay|"
    seasonData_years.shift();

    // get stats for each of the years in the table
    const statsOut = {}; 
    /* ex: statsOut.statsByYear.2020.stats.GP = [50, 10]
           statsOut.statsByYear.2020.stats.teams = ['yankees', 'tigers']
    */
    statsOut.statsByYear = {};
    const allStats = ['GP', 'GS', 'MPG', 'FG%', '3P%', 'FT%', 'RPG', 'APG', 'SPG', 'BPG', 'PPG'];
    const intRegex = /([0-9]+)/g; // matches every occurrence of at least 1 digit
    let teamIndex = 0;
    for (const yearChunk of seasonData_years) {  // visits each row
        // what year is it?
        const year = yearChunk.slice(0, 4);

        // check if this is a row with a new year. If so, init a new empty year
        if (!statsOut.statsByYear[year]) {
            teamIndex = 0;
            // init the new year's arrays so data can be appended
            statsOut.statsByYear[year] = {stats: []};
            statsOut.statsByYear[year].stats[teamIndex] = {};
        } else {
            // this is a new row because of a team change, so it continues the same year
            ++teamIndex;
            statsOut.statsByYear[year].stats[teamIndex] = {};
        }

        // split on the word "season" to get team name section to the left of "season"
        const seasonSplit = yearChunk.split('season')[0];

        // get the char that sandwiches the team name on the left
        const digitsInChunk = seasonSplit.match(intRegex);
        const leftSandwich = digitsInChunk.pop();

        if (!leftSandwich) {
            continue;
        }

        // get the char that sandwiches the team name on the right
        const rightSandwich = 'season';
        
        // get the indices of the word "season" and the preceding digit. These sandwich the team name
        const indexOfSeason = yearChunk.lastIndexOf(rightSandwich);
        const indexOfLastDigit = seasonSplit.lastIndexOf(leftSandwich);

        // add the team name to the list of team names
        const team = yearChunk.slice(indexOfLastDigit + 2, indexOfSeason);  // TODO: test with multiple teams in one year
        statsOut.statsByYear[year].stats[teamIndex].team = team;

        // split off the last 2 rows, which are special "Career" and "All Star"
        const firstColspanIndex = yearChunk.indexOf('colspan=\"2');
        const normalRows = yearChunk.slice(0, firstColspanIndex);
        const specialRows = yearChunk.slice(firstColspanIndex, yearChunk.length - 1);

        // split on double bars ( || ) to get [junk+firstStat, stat, ..., stat, lastStat+junk]
        const statsSplit = normalRows.split('||');

        // save stats using the corresponding name of each stat
        // allStats and statsSplit are in the same order
        for (let index = 0; index < statsSplit.length; ++index) {
            // get the stat name
            const statName = allStats[index];

            // get the stat
            let stat;
            let statChunk = statsSplit[index];

            // first and last chunks have other stuff to discard
            if (index === 0) {
                // keep only the number by getting it from between last pipe to end of chunk
                const lastPipeIndex = statChunk.lastIndexOf('|');                
                stat = statChunk.slice(lastPipeIndex, statChunk.length - 1);
            } else if (index === statsSplit.length - 1) {
                // discard stuff after the stat number
                const newlineIndex = statChunk.indexOf('\n');
                stat = statChunk.slice(0, newlineIndex);
            } else {
                // nothing to discard
                stat = statChunk;
            }
            
            // some stats have clutter (like ''' or a style attribute) to denote achievements
            let statArray = stat.split(''); // split string into char array
            // filter out anything that isn't a number or decimal point
            const statChars = ['.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
            statArray = statArray.filter((char) => {
                let isApproved = statChars.includes(char)
                // console.log(char, ' is approved? ', isApproved);
                return isApproved;
            });
            // keep the re-stringified result
            stat = statArray.join('');

            // cast to float if it's a number stat (all but team name are #)
            if (statName !== 'team') {
                stat = Number(stat);
            }
            statsOut.statsByYear[year].stats[teamIndex][statName] = stat;
        }
    }

    console.log('\n\ngot stats');
    for (const year of Object.keys(statsOut.statsByYear)) {
        console.log('\n' + year + '... ');
        for (const teamStats of statsOut.statsByYear[year].stats) {
            for (const stat of Object.keys(teamStats)) {
                console.log(stat + ': ' + teamStats[stat]);
            }
        }
    }
    /* ==============================================================
        2. get awards player won during given year 
    */
    // get section index of player's NBA awards section (got that above)
    // request the awards section of the player's page
    if (typeof sectionIndex_awards === 'undefined') {
        console.log('no awards page found. Returning stats though');
        // label all years with "no awards" message
        for (const year of Object.keys(statsOut.statsByYear)) {
            statsOut.statsByYear[year].awards = ['NONE_FOUND'];
        }
    } else {
        queryString = queryStringBase + '&page=' + playerNameUrlized + '&prop=wikitext&section=' + sectionIndex_awards;
        const awardResponse = await makeWikiApiGet(queryString);
        if (!awardResponse || typeof awardResponse.parse === 'undefined' || typeof awardResponse.parse.wikitext === 'undefined') {
            console.log('no awards page found. Returning stats though');
            console.log('\tthis is the whole response that did\'nt work:\n', awardResponse);
            console.log('\tthis is the querystring: ', queryString);
            // label all years with "no awards" message
            for (const year of Object.keys(statsOut.statsByYear)) {
                statsOut.statsByYear[year].awards = ['NONE_FOUND'];
            }
        } else {
            const awardsData = awardResponse.parse.wikitext['*'];
        
            // split off the header TODO: currently gets whole achievements section. Team wants only "Regular season"?
        
            // parse awards bullet point list
            const cleanAwardsData = await parseWikiAwardsBullets(awardsData);
            for (let awardData of cleanAwardsData) {
                for (const year of awardData.years) {
                    // console.log('\tadding "', awardData.cleanString , '" award for ', year);
                    if (statsOut.statsByYear[year]) {
                        if (typeof statsOut.statsByYear[year].awards !== 'undefined') {
                            // append to existing year's awards list
                            statsOut.statsByYear[year].awards.push(awardData.cleanString);
                        } else {
                            // first award found this year; need to add 'awards' property to this year
                            statsOut.statsByYear[year].awards = [awardData.cleanString];
                        }
                    }
                }
            }

            // enforce that every year has .awards = []
            for (const year of Object.keys(statsOut.statsByYear)) {
                if (typeof statsOut.statsByYear[year].awards === 'undefined' || !Array.isArray(statsOut.statsByYear[year].awards)) {
                    // this year didn't have any awards, so assign a message, in the expected data type
                    statsOut.statsByYear[year].awards = ['NONE_FOUND'];
                }
            }

            console.log('\n\ngot awards:');
            for (const year of Object.keys(statsOut.statsByYear)) {
                console.log('\t', year, ': ', statsOut.statsByYear[year].awards);
            }
        }
    }

    /* ==============================================================
        3. get all-time achievements player won
    */
    // Find “line”: “NBA achievements” and get the value of its index. (got that above)
    // TODO: to actually use the career achievements page, would need to repeat first query 
    // and first sectionIndex finding loop. Currently this undefined check always skips this part
    if (typeof sectionIndex_achievements === 'undefined') {
        console.log('\nno achievements page found. Returning stats though');
        statsOut.achievements = 'NONE_FOUND';
    } else {
        queryString = queryStringBase + '&page=List_of_career_achievements_by_' + playerNameUrlized + '&prop=text&section=' + sectionIndex_achievements;
        const achievementResponse = await makeWikiApiGet(queryString);
        if (!achievementResponse || typeof achievementResponse.parse === 'undefined') {
            console.log('\nno achievements page found. Returning stats though');
            console.log('\tthis is the whole response that did\'nt work:\n', achievementResponse);
            console.log('\tthis is the querystring: ', queryString);
            statsOut.achievements = 'NONE_FOUND';
        } else {
            const achievementData = achievementResponse.parse.wikitext['*'];
        
            // split off the header TODO: currently gets whole achievements section. Team wants only "Regular season"?
        
            // parse bullet point list
            statsOut.achievements = await parseWikiAchievementsBullets(achievementData);
        
            console.log('\n\ngot achievements:', statsOut.achievements);
        }
    }

    // return the object
    return statsOut;
}


async function stripYears(inputText) {
    /* 
    extracts numbers in {{...####...}} from a string
    inputText: any string
    return: object with:
        years: array of numbers; the years in the input
        cleanString: the input string without the {{...}}
    */
    // console.log('\n\t\t stripping years from ', inputText);
    const output = {
        years: [],
        cleanString: ''
    }

    // split on {{ so groups are '[[NBA Award]]:', '{{nbay|2020|end}}'
    const braceSplitRE = /(?={{)/g;
    const braceSplit = inputText.split(braceSplitRE);

    // if a section contains {{, find "|####" and save the #### for return
    for (let chunk of braceSplit) {
        // console.log('\t\tremoving {{...}} from ', chunk);
        if (chunk[0] === '{') {
            // keep the #### from "|####"
            const yearRE = /\|[0-9]+/;
            const pipeAndYear = chunk.match(yearRE)[0];
            let year = pipeAndYear.slice(1, pipeAndYear.length);
            year = Number(year);  // cast to number
            output.years.push(year);

            // destroy the {{...}} by ignoring this chunk
        } else if (chunk[0] !== '}'){  // handle last string of split
            // this is a good clean non-{{...}} chunk. Add to output!
            output.cleanString += chunk;
        }
    }

    // console.log('\t\treturning this clean BRACELESS awards bullet text: ', output.cleanString);
    // console.log('\t\twith years: ', output.years);
    return output;
}


async function removeDoubleBrackets(inputText) {
    /*
    removes [[ and ]] from a string and returns the new string
    param inputText: any string
    return: inputText, minus the [[ and ]]
    */
    // split on double bracket (keeping opening brackets)
    const bracketSplitRE = /(?=\[\[)|\]\]/g;
    const bracketSplit = inputText.split(bracketSplitRE);
    // console.log('\n\nbracket split:\n', bracketSplit);

    // rejoin string, discarding [[ ... |
    let bracketlessText = '';
    for (let chunk of bracketSplit) {
        // debug
        // console.log('\tremoving [[...| from: ' + chunk);

        // if there is a pipe, discard everything up to the pipe
        const indexOfPipe = chunk.indexOf('|');
        if (indexOfPipe !== -1) {
            chunk = chunk.slice(indexOfPipe + 1, chunk.length);
        }

        // if the first char is a [, remove the opening [[
        if (chunk[0] === '[') {
            chunk = chunk.slice(2, chunk.length);
        }

        // add this freshly cleaned text to output
        bracketlessText += chunk;
        // console.log('\tafter removal: ' + chunk);
    
        // remove closing ]]
    }

    // console.log('cleaned text result: ' + bracketlessText);
    return bracketlessText;
}


async function parseWikiAwardsBullets(awardData) {
    /* 
    param data: string; wiki-formatted bullet point list
    return: object with list of non-wiki strings and numbers for award years
        output = [
            {
                cleanString: 'mvp guy award',
                years: [2020, 2011]
            },
        ]
    */
    // split on outer bullet ('\n*') OR any level nested bullet ('\n*******')
    const bulletSplitRE = /\n\**/g;
    const bulletSplit = awardData.split(bulletSplitRE);

    // add bullets to the return array
    listOut = [];
    for (let bulletText of bulletSplit) {
        const awardDataOut = {};
        // console.log('cleaning bullet ' + bulletText);
        // if this is a nested bullet, TODO!

        // if this bullet has a <ref> tag, cut it out (and everything after it)
        const indexOfRefTag = bulletText.indexOf('<ref');
        if (indexOfRefTag) {
            bulletText = bulletText.slice(0, indexOfRefTag);
        }

        // extract years and remove their {{...}} trappings
        const {years, cleanString} = await stripYears(bulletText);
        bulletText = cleanString;
        awardDataOut.years = years;

        // cut out [[ and ]]
        bulletText = await removeDoubleBrackets(bulletText);

        // done cleaning this bullet
        awardDataOut.cleanString = bulletText;
        listOut.push(awardDataOut);
        // console.log('\treturning this clean award: ', bulletText);
        // console.log('\treturning these years: ', years);
    }

    return listOut;
}


async function parseWikiAchievementsBullets(awardData) {
    /* 
    param data: string; wiki-formatted bullet point list
    return: list of strings, not wiki formatted
    */
    // split on outer bullet ('\n*') OR any level nested bullet ('\n*******')
    const bulletSplitRE = /\n\**/g;
    const bulletSplit = awardData.split(bulletSplitRE);

    // add bullets to the return object
    listOut = [];
    for (let bulletText of bulletSplit) {
        // if this bullet has a <ref> tag, cut it out (and everything after it)
        const indexOfRefTag = bulletText.indexOf('<ref>');
        if (indexOfRefTag) {
            bulletText = bulletText.slice(0, indexOfRefTag);
        }

        // if this is a nested bullet, TODO!

        // split on double bracket (keeping opening brackets)
        const bracketSplitRE = /(?=\[\[)|\]\]/g;
        const bracketSplit = bulletText.split(bracketSplitRE);

        // debug:
        // console.log('\n\nbracket split:\n', bracketSplit);

        // rejoin string, discarding [[ ... |
        let cleanedBulletText = '';
        for (let chunk of bracketSplit) {
            // debug
            // console.log('\nstarting chunk: ' + chunk);

            // if there is a pipe, discard everything up to the pipe
            const indexOfPipe = chunk.indexOf('|');
            if (indexOfPipe !== -1) {
                chunk = chunk.slice(indexOfPipe + 1, chunk.length);
                // console.log('\n\tthis one has a pipe. New chunk: ' + chunk);
            }

            // if the first char is a [, remove the opening [[
            if (chunk[0] === '[') {
                chunk = chunk.slice(2, chunk.length);
                // console.log('\n\tthis one has a bracket. New chunk: ' + chunk);
            }

            // add this freshly cleaned bullet point to the output variable
            // console.log('cleaned?\t' + cleanedBulletText);
            cleanedBulletText += chunk;
        }

        // add the cleaned bullet to the return object
        listOut.push(cleanedBulletText);
    }

    return listOut;
}


async function makeWikiApiGet(queryString) {
    /*
    makes a GET request to the wikipedia API given the query string
    adapted from https://www.twilio.com/blog/5-ways-to-make-http-requests-in-node-js-using-async-await
    queryString: string like 'action=parse&format=json&page=List_of_career_achievements_by_Stephen_Curry&prop=sections'
    return: response data, or false if there was an error
    */

    // define options for request and callback for response receipt
    try {
        const response = await axios.get('https://en.wikipedia.org/w/api.php?' + queryString);
        // for debug:
        // console.log('\n\nreceived first response in makeWikiApiGet:\n', response.data);
        return response.data;
    } catch (error) {
        console.log(error.response.body);
        return false;
    }
}

async function handlePlayerStatsReq(playerName, requestYear) {
    /* used for shared functionality between API and GUI */
    console.log('received request for year ' + requestYear);

    // get requested data (or false return if requested data isn't available)
    const result = await getWikiStats(playerName);
    // console.log('\n\nafter return:' + result);

    // build context object with result to prepare for response
    let context = {};
    if (result === false) {
        // the requested player has no stats. Send a response with only an errorMessage
        console.log('\npage does not exist for that player name, sending responseText: NO_PLAYER_FOUND');
        context.errorMessage = 'NO_PLAYER_FOUND';
    } else {
        // the requested player has stats...
        console.log('\ngetting year ' + requestYear + ' from this data for all years:\n', result.statsByYear);
        const requestedYearResult = result.statsByYear[requestYear];
    
        if (!requestedYearResult) {
            // year was invalid, but there is data for other years
            console.log('player\'s page was found, but year was not given or there was no data for the requested year. Sending a list of available years');
            if (typeof requestYear !== 'undefined') {
                // year was requested but not available
                context.errorMessage = 'YEAR_NOT_FOUND';
            }
            // return an object with only a list of available years
            context.years = Array.from(Object.keys(result.statsByYear));
        } else {
            // the requested player's stats were obtained!
            context = requestedYearResult;
            // context.achievements = result.achievements;
        }
    }
    // console.log('after getWikiStats, returning these stats to route handler:\n', context.stats);
    // console.log('first team\'s stats of which is: ', context.stats[0]);
    return context;
}

async function makeUsageStatGET(prefix, path) {
    /*just returns response.data or false if there's a problem*/
    try {
        const response = await axios.get('https://' + prefix + '.loafing.dev' + path);
        // console.log('received usage stats response from ' + prefix);
        return response.data;
    } catch (error) {
        console.log('ERROR from ' + prefix + '. Got this error.response.body: ', error.response.body);
        return false;
    }
} 

app.get('/', (req, res) => {
    res.render('home');
})

.get('/player', async (req, res) => {
    // inspect request
    const playerName = req.query.firstname + ' ' + req.query.lastname;
    const requestYear = req.query.year;

    // get requested data (or false return if requested data isn't available)
    const context = await handlePlayerStatsReq(playerName, requestYear);
    console.log('sending this context:\n', context);
    res.send(context);
    // res.render('home', context);
})

.get('/api/player', async (req, res) => {
    // exposes identical data as /player but only the player data, without stuff needed for GUI
    // inspect request
    const playerName = req.query.firstname + ' ' + req.query.lastname;

    // get requested data (or false return if requested data isn't available)
    const context = await handlePlayerStatsReq(playerName);
    if (context.errorMessage) {
        res.status(400);
        res.send(context.errorMessage);
    } else {
        console.log('sending this context:\n', context);
        res.send(context);
    }
})

.get('/api/player/year/:reqYear', async (req, res) => {
    // exposes identical data as /player but only the player data, without stuff needed for GUI
    // inspect request
    const playerName = req.query.firstname + ' ' + req.query.lastname;
    const requestYear = req.params.reqYear;

    // get requested data (or false return if requested data isn't available)
    const context = await handlePlayerStatsReq(playerName, requestYear);
    if (context.errorMessage) {
        // year was given but invalid
        res.status(400);
        res.send(context.errorMessage);
    } else {
        console.log('sending this context:\n', context);
        res.send(context);
    }
})

.get('/api/roster', async (req, res) => {
    // returns a list of players who played for a given team during a given year
    const result = await getRosterFromWiki(req.query.teamcity, req.query.teamname, req.query.year);
    if (result.errorMessage === 'BAD_PARSE') {
        // team page was found but player sections didn't exist or couldn't be parsed
        res.status(500);
        res.send(result.errorMessage);
    } else if (result.errorMessage) {
        // some req param was invalid
        res.status(400);
        res.send(result.errorMessage);
    } else {
        const context = {};
        context.roster = result.playersOut;
        context.teamcity = req.query.teamcity;
        context.teamname = req.query.teamname;
        console.log('sending this context:\n', context);
        res.send(context);
    }
})

.get('/apiReport', (req, res) => {
    res.render('apiReport');
})

.get('/api/api-usage', async (req, res) => {
    // make a cross-server request to other microservices' endpoints specifically set up to receive this request
    const responses = {
        testServiceGood: {nba: {queries: 50}, wnba: {queries: 11}},
        testServiceBad: {nba: {queries: -1}, wnba: {queries: -1}},
    };

    // get api usage data from other microservice
    const prefixes = ['rosterhistory'];
    const paths = ['/api/usage-stats'];

    for (let index = 0; index < prefixes.length; ++index) {
        const prefix = prefixes[index];
        const path = paths[index];
        // get response.data if available
        const serviceResponse = await makeUsageStatGET(prefix, path);

        // initialize entry for this service
        responses[prefix] = {
            nba: {},
            wnba: {}
        };

        if (serviceResponse) {
            // add response.data to a list
            responses[prefix].nba.queries = serviceResponse.NBA.queries;
            responses[prefix].wnba.queries = serviceResponse.WNBA.queries;
        } else {
            // -1 is error code
            responses[prefix].nba.queries = -1;
            responses[prefix].wnba.queries = -1;
        }
    }

    // send response with data
    res.send(responses);
});

// if none of the above were triggered, the page doesn't exist or support the request verb
app.use((req, res) => {
    res.status(404);
    res.send('404');
  });

// if none of the above were triggered, there was a server-side syntax error
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500);
  res.send('500');
});

// start listening
app.listen(app.get('port'), () => {
  console.log('Express listening on the current server from port' + app.get('port') + '; press Ctrl-C to terminate.');
});